﻿using System.Windows;
using Server.BLL;

namespace Server
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ChatServer server;

        public MainWindow()
        {
            InitializeComponent();
            server = new ChatServer();

            ActiveClientsListBox.SelectionChanged += (sender, args) =>
            {
                if (ActiveClientsListBox.SelectedValue == null)
                {
                    return;
                }

                if (ActiveClientsListBox.SelectedValue is Client)
                {
                    TargetUsername.Text = (ActiveClientsListBox.SelectedValue as Client).Username;
                }
            };

            DataContext = server;
        }

        private void SendMessageButton_OnClick(object sender, RoutedEventArgs e)
        {
            server.SendMessage(TargetUsername.Text, MessageToSend.Text);
        }

        private void StartStopButton_OnClick(object sender, RoutedEventArgs e)
        {
            server.SwitchState();
        }
    }
}
