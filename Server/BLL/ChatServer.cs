﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Windows.Threading;
using Server.Annotations;

namespace Server.BLL
{
    public class ChatServer : INotifyPropertyChanged
    {
        private Dispatcher Dispatcher { get; set; }
        private Thread Thread { get; set; }
        private Socket Socket { get; set; }
        private IPAddress IpAddr { get; set; }
        public string IpAddress { get; set; }
        public int Port { get; set; }
        public IPEndPoint IpEndPoint => new IPEndPoint(IpAddr, Port);
        public ObservableCollection<Client> Clients { get; set; }
        public ObservableCollection<string> Messages { get; set; }
        public int ClientIdCounter { get; set; }
        public string Username { get; set; }
        public bool IsConnected => SocketUtils.IsConnected(Socket);
        public bool IsActive { get; set; } = false;
        public bool IsNotActive => !IsActive;
        public int ActiveClients => Clients.Count;

        public ChatServer()
        {
            Dispatcher = Dispatcher.CurrentDispatcher;
            IpAddress = "127.0.0.1";
            IpAddr = IPAddress.Parse(IpAddress);
            Port = 5960;
            Username = "Server";

            Clients = new ObservableCollection<Client>();
            Messages = new ObservableCollection<string>();

            Clients.CollectionChanged += (sender, args) => OnPropertyChanged(nameof(ActiveClients)); 

            ClientIdCounter = 0;
        }

        private void Start()
        {
            if (IsActive)
            {
                return;
            }

            Socket = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);
            Socket.Bind(IpEndPoint);
            Socket.Listen(5);

            Thread = new Thread(WaitForConnections);
            Thread.Start();

            Clients.Add(new Client() {Id = 0, Username = Username});

            IsActive = true;
            OnPropertyChanged(nameof(IsActive));
            OnPropertyChanged(nameof(IsNotActive));
        }

        private void Stop()
        {
            if (!IsActive)
            {
                return;
            }

            while (Clients.Count != 0)
            {
                Client client = Clients[0];
                Clients.Remove(client);
            }

            Socket.Dispose();
            Socket = null;

            IsActive = false;
            OnPropertyChanged(nameof(IsActive));
            OnPropertyChanged(nameof(IsNotActive));
        }

        public void SwitchState()
        {
            if (!IsActive)
            {
                Start();
            }
            else
            {
                Stop();
            }
        }

        private void WaitForConnections()
        {
            while (true)
            {
                if (Socket == null)
                {
                    return;
                }

                Client client = new Client
                {
                    Id = ClientIdCounter,
                    Username = "Client" + ClientIdCounter,
                    Socket = Socket.Accept()
                };

                client.Thread = new Thread(() => ProcessMessages(client));

                Dispatcher.Invoke(new Action(() =>
                    Clients.Add(client)), null);

                client.Thread.Start();
                ClientIdCounter++;
            }
        }

        private void ProcessMessages(Client client)
        {
            while (true)
            {
                if (client.IsConnected() == false)
                {
                    Dispatcher.Invoke(() =>
                    {
                        Clients.Remove(client);
                        client.Dispose();

                    });
                    return;
                }

                var messageBytes = new byte[1024];
                var receivedBytes = client.Socket.Receive(messageBytes);

                var messageReceived = Encoding.Unicode.GetString(messageBytes);

                if (receivedBytes <= 0) continue;

                if (messageReceived.Substring(0, 8) == "/setname")
                {
                    var newUsername = messageReceived.Replace("/setname ", "").Trim('\0');
                    client.Username = newUsername;
                    Clients.CollectionChanged += (sender, args) => OnPropertyChanged(nameof(client.Username));
                }
                else if (messageReceived.Substring(0, 6) == "/msgto")
                {
                    var data = messageReceived.Replace("/msgto ", "").Trim('\0');
                    var targetUsername = data.Substring(0, data.IndexOf(':'));
                    var messageToSend = data.Substring(data.IndexOf(':') + 1);

                    Dispatcher.Invoke(new Action(() =>
                        {
                            SendMessage(client, targetUsername, messageToSend);
                            
                        }
                    ), null);

                    Dispatcher.Invoke(new Action(() =>
                        {
                            SendMessage(client, client.Username, messageToSend);

                        }
                    ), null);
                }
            }
        }

        private void SendMessage(Client senderClient, string receiverUsername, string messageContent)
        { 
            var message = "You:" + messageContent;
            
            if (!senderClient.Username.Equals(receiverUsername))
            {
                message = senderClient.Username + ":" + messageContent;
                var messageLog = $"**LOG** From: {senderClient.Username} | To: {receiverUsername} | Message: {messageContent}";
                Messages.Add(messageLog);
            }

            var isSent = false;

            foreach (var client in Clients)
            {
                if (!client.Username.Equals(receiverUsername)) continue;

                client.SendMessage(message);
                isSent = true;
            }

            if (isSent == false)
            {
                senderClient.SendMessage("**SERVER** ERROR! Username not found! Your message was not sent!");
            }
        }

        public void SendMessage(string receiverUsername, string messageContent)
        {
            SendMessage(Clients[0], receiverUsername, messageContent);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
