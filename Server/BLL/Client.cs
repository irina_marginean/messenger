﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Server.BLL
{
    public class Client : IDisposable
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public Socket Socket { get; set; }
        public Thread Thread { get; set; }

        public void SendMessage(string message)
        {
            if (IsConnected())
            {
                Socket.Send(Encoding.Unicode.GetBytes(message));
            }
        }

        public bool IsConnected()
        {
            return SocketUtils.IsConnected(Socket);
        }

        public void Dispose()
        {
            Socket?.Shutdown(SocketShutdown.Both);
            Socket?.Dispose();
            Thread = null;
        }
    }
}
