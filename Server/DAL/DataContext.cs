﻿using Microsoft.EntityFrameworkCore;
using Server.Models;

namespace Server.DAL
{
    public class DataContext : DbContext
    {
        public DataContext() : base() { }

        public DbSet<User> Users { get; set; }
        public DbSet<Message> Messages { get; set; }

    }
}
