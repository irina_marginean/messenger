﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Models
{
    public class Message
    {
        public string MessageContent { get; set; }
        [Key, Required]
        public string SenderUsername { get; set; }
        [Key, Required]
        public string ReceiverUsername { get; set; }
        public DateTime DateTimeSent { get; set; }
    }
}
